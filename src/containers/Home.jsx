//dependencies
import React, { useState } from 'react';

//styles & images
import styles from './Home.module.scss'
import BgImage from '../assets/images/bg.jpg'
import { FiLogOut } from 'react-icons/fi';
import Carousel from '../components/Carousel';
import Property1 from '../assets/images/property1.png';
import Property2 from '../assets/images/property2.jpeg';
import Property3 from '../assets/images/property3.jpeg';
import Property4 from '../assets/images/property4.jpeg';
import Property5 from '../assets/images/property5.jpeg';
import Property6 from '../assets/images/property6.jpeg';
import ProfileModal from '../components/Modals/ProfileModal';
import { Row, Col } from 'reactstrap';

const categories = [
  'Houses & Villas',
  'Apartments',
  'Builder Floors',
  'Farm Houses'
]

const PropertyDeals = [
  { title: '2 BHK nice place to own', location: 'Surat', img: Property1, price: 2700000 },
  { title: 'fully furnished house', location: 'Delhi', img: Property2, price: 1230000 },
  { title: 'House for sell', location: 'Mumbai', img: Property3, price: 4530000 },
  { title: 'Loan approved house', location: 'Kolkata', img: Property4, price: 8570000 },
  { title: 'luxurious flat, Delhi', location: 'Delhi', img: Property5, price: 1299990 },
  { title: 'Near surat Airport, good place', location: 'Surat', img: Property6, price: 4568000 },
]

export default () => {

  //state for modal
  const [isProfileModalVisible, setIsProfileModalVisible] = useState(false)

  const toggleProfileModal = () => setIsProfileModalVisible(!isProfileModalVisible);

  var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'INR',

    // These options are needed to round to whole numbers if that's what you want.
    //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
    //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
  });

  return (
    <div className={styles.main}>

      {/* first header */}
      <div className={styles.header1}>
        <p className={styles.appName}>Property Hub</p>
        <div className={styles.rightSection}>
          <div className={styles.profile} onClick={toggleProfileModal}>
            <img src={BgImage} className={styles.profileImg} />
            <div>
              <p className={styles.username}>Pavan Tank</p>
              <p className={styles.viewProfile}>View Profile</p>
            </div>
          </div>
          <div className={styles.logout}>
            <FiLogOut className={styles.logoutIcon} />
            <p>Logout</p>
          </div>
        </div>
      </div>

      {/* Carousel */}
      <Carousel />

      {/* section 1 */}
      <div className={styles.section1}>
        <div className={styles.sideBar}>
          <p className={styles.sectionTitle}>All Categories</p>
          {categories.map(ele => {
            return (
              <div className={styles.categories}>
                <input type="checkbox" />
                <p>{ele}</p>
              </div>
            )
          })}
        </div>
        <div className={styles.MainArea}>
          <p className={styles.sectionTitle}>Recently Added Property Deals for you</p>
          <div className={styles.propertyListing}>
            {PropertyDeals.map((ele, index) => {
              return (
                <div className={styles.listingItem}>
                  <img src={ele.img} className={styles.img} />
                  <p className={styles.price}>{formatter.format(ele.price)}</p>
                  <p className={styles.title}>{ele.title}</p>
                  <p className={styles.location}>{ele.location}</p>
                </div>
              )
            })}
          </div>
          <p className={styles.sectionTitle} style={{ marginTop: 50 }}>Top Deals for you</p>
          <div className={styles.propertyListing}>
            {PropertyDeals.slice(0).reverse().map((ele, index) => {
              return (
                <div className={styles.listingItem}>
                  <img src={ele.img} className={styles.img} />
                  <p className={styles.price}>{formatter.format(ele.price)}</p>
                  <p className={styles.title}>{ele.title}</p>
                  <p className={styles.location}>{ele.location}</p>
                </div>
              )
            })}
          </div>
        </div>
      </div>

      {/* footer */}
      <div className={styles.footerMain}>
        <Row>
          <Col lg={2} />
          <Col lg={2}>
            <p className={styles.title}>Property Hub Details</p>
            <p className={styles.help}>About Us</p>
            <p className={styles.help}>Terms & Conditions</p>
            <p className={styles.help}>Contact Us</p>
            <p className={styles.help}>Privacy Policy</p>
            <p className={styles.help}>Help</p>
            <p className={styles.help}>Logout</p>
          </Col>
          <Col lg={2}>
            <p className={styles.title}>Top Content For you</p>
            <p className={styles.help}>Recently Added Properties</p>
            <p className={styles.help}>Top Deals for you</p>
            <p className={styles.help}>Most visited Properties</p>
            <p className={styles.help}>Explore Categories</p>
            <p className={styles.help}>Location wise Deals</p>
          </Col>
          <Col lg={2}>
            <p className={styles.title}>Contact Us</p>
            <p style={{ color: 'grey' }}>
              We're always Available for you and your concern, We're here to help you to find your dream property. never hesitate to reach us.
              </p>
            <p className={styles.help}>Email: propertyhub11@gmail.com</p>
          </Col>
          <Col lg={2}>
            <p className={styles.appName}>Property Hub</p>
            <p className={styles.slogan}>Your mate to find you The Best,<br /> Most Suitable and Dream<br /> Property &#128522;</p>
          </Col>
          <Col lg={2} />
        </Row>
      </div>
      <div className={styles.subFooter}>
        property hub india © 2021
      </div>

      {/* profile modal */}
      <ProfileModal isProfileModalVisible={isProfileModalVisible} toggle={toggleProfileModal} />
    </div>
  )
}