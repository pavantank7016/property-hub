//dependencies
import React from 'react';

//styles, icons & images
import BgImage from '../assets/images/bg.jpg'
import InputField from '../components/InputField';
import styles from './Login.module.scss';
import { AiFillFacebook } from 'react-icons/ai'
import { FcGoogle } from 'react-icons/fc'

export default () => {
  return (
    <div className={styles.main}>
      <img src={BgImage} className={styles.bgImage} />

      <div className={styles.centerBox}>
        <p className={styles.title}>Property Hub</p>
        <p className={styles.subTitle}>Sign In</p>

        <InputField
          placeholder="Your Name"
          leftIconMode="email"
        />

        <InputField
          placeholder="Password"
          leftIconMode="password"
          rightIconMode="eye"
        />

        <div className={styles.rememberMe}>
          <input type="checkbox" className={styles.checkbox} />
          <label>Remember me</label>
        </div>

        <div className={styles.bottomSection}>
          <div className={styles.button}>Sign In</div>
          <div className={styles.socialIconLogin}>
            <span>or Login with</span>
            <AiFillFacebook className={styles.socialIcon} style={{fill: '#4267B2'}}/>
            <FcGoogle className={styles.socialIcon} style={{height: '22px', width: '22px'}}/>
          </div>
        </div>
      </div>
    </div>
  )
};