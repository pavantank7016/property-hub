//dependencies
import React, { useState } from 'react';
import { AiFillCamera } from 'react-icons/ai';
import { Col, Modal, ModalHeader, ModalBody, Button, ModalFooter, Row, Form, Input, Label, FormGroup, CustomInput } from 'reactstrap';

//styles & images
import BgImage from '../../assets/images/bg.jpg'

export default ({ isProfileModalVisible, toggle }) => {
  return (
    <Modal isOpen={isProfileModalVisible} toggle={toggle}>
      <ModalHeader toggle={toggle}>Modal title</ModalHeader>
      <ModalBody>
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          marginBottom: '30px'
        }}>
          <img src={BgImage}
            style={{
              height: '150px',
              width: '150px',
              borderRadius: '50%'
            }} />
          <p style={{
            color: 'grey',
            cursor: 'pointer'
          }}>
            Edit Profile picture
          </p>
        </div>
        <Form>
          <FormGroup row>
            <Label for="exampleName" sm={2}>Name</Label>
            <Col sm={10}>
              <Input type="text" name="name" id="exampleName" placeholder="Full Name" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="exampleEmail" sm={2}>Email</Label>
            <Col sm={10}>
              <Input type="text" name="email" id="exampleEmail" placeholder="Email Address" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="exampleContact" sm={2}>Contact</Label>
            <Col sm={10}>
              <Input type="text" name="email" id="exampleContact" placeholder="Contact Number" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="exampleAddress" sm={2}>Address</Label>
            <Col sm={10}>
              <Input type="text" name="email" id="exampleAddress" placeholder="Address" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="exampleSelect" sm={2}>Role</Label>
            <Col sm={10}>
              <Input type="select" name="select" id="exampleSelect">
                <option>Owner</option>
                <option>Dealer</option>
                <option>Builder</option>
              </Input>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="exampleCheckbox" sm={2}>Gender</Label>
            <Col sm={10}
              style={{
                display: 'flex',
                alignItems: 'center'
              }}>
              <CustomInput type="checkbox" id="exampleCustomInline" label="Male" inline />
              <CustomInput type="checkbox" id="exampleCustomInline2" label="Female" inline />
            </Col>
          </FormGroup>
        </Form>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={toggle}>Save Changes</Button>{' '}
        <Button color="secondary" onClick={toggle}>Cancel</Button>
      </ModalFooter>
    </Modal>
  )
}