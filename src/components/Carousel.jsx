//dependencies
import React, {useState} from 'react';
import Carousel, { Dots } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';

//styles & images
import styles from './Carousel.module.scss';
import BgImage from '../assets/images/bg.jpg'
import Property1 from '../assets/images/property1.png';
import Property2 from '../assets/images/property2.jpeg';
import Property3 from '../assets/images/property3.jpeg';
import Property4 from '../assets/images/property4.jpeg';

export default () => {
  const [value, setValue] = useState(0);

  const onChange = value => {
  setValue(value);
  }

  return (
    <div>
      <Carousel
        value={value}
        onChange={onChange}
      >
        <img className={styles.carouselItem} src={Property1} />
        <img className={styles.carouselItem} src={Property2} />
        <img className={styles.carouselItem} src={Property3} />
        <img className={styles.carouselItem} src={Property4} />
      </Carousel>
      <Dots
        value={value}
        onChange={onChange}
        thumbnails={[
          (<img key={1} className={styles.preview} src={Property1} />),
          (<img key={2} className={styles.preview} src={Property2} />),
          (<img key={3} className={styles.preview} src={Property3} />),
          (<img key={4} className={styles.preview} src={Property4} />),
        ]}
      />
    </div>
  )
}