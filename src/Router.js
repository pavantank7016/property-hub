//dependencies
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

//components
import Login from './containers/Login'
import Home from './containers/Home'

const MainRouter = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Login}/>
        <Route path="/home" component={Home}/>
      </Switch>
    </Router>
  )
}

export default MainRouter;
